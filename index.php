<?php 
// link: https://www.linkedin.com/in/ugursekin/
// belirli fiyat aralıklarındaki, istenilen dil de ki kursları getir.

function getUdemyCourses($price_min = 50, $price_max = 150, $language = 'en') {
    $access_token = "your_access_token_here";
    $url = "https://www.udemy.com/api-2.0/courses/?fields[course]=title,url,price_image&price[price]=[" . $price_min . "," . $price_max . "]&ordering=highest-rated&instructional_level=some-college-level&language=" . $language . "&page_size=10";
    $headers = array(
        "Authorization: Bearer " . $access_token,
        "Content-Type: application/json",
    );
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($response, true);
    $courses = array();
    foreach ($json['results'] as $course) {
        $courses[] = array(
            'title' => $course['title'],
            'url' => $course['url'],
            'price_image' => $course['price_image']['price_detail']['amount'],
        );
    }
    return $courses;
}


?>